﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq.Mapping;

namespace gallery.Models
{
    [Table(Name = "FEEDBACK_TABLE")]
    public class Feedback
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column(Name = "Name")]
        public string Name { get; set; }
        [Column(Name = "Email")]
        public string Email { get; set; }
        [Column(Name = "Subject")]
        public string Subject { get; set; }
        [Column(Name = "Body")]
        public string Body { get; set; }

    }
}