﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(gallery.Startup))]
namespace gallery
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
