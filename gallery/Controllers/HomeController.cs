﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq;
using gallery.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace gallery.Controllers
{
    public class HomeController : Controller
    {

        public static string SQLconnectionString = "Server = tcp:galleryti42.database.windows.net,1433;Initial Catalog = gallery; Persist Security Info=False;User ID = student; Password=09ADS12ljk; MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout = 30";
        DataContext SQLdb = new DataContext(SQLconnectionString);

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Pict()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Feedback(string name, string email, string subject, string message)
        {
            Feedback fb = new Feedback() // MSSQL
            {
                Name = name,
                Email = email,
                Subject = subject,
                Body = message
            };
            //SaveFB(fb).GetAwaiter();

            SQLdb.GetTable<Feedback>().InsertOnSubmit(fb);
            SQLdb.SubmitChanges();
            ViewBag.Message = "Your application description page.";

            return View("Contact");
        }

        public ActionResult Contact()
        {
    
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult Gallery()
        {
            ViewBag.Message = "Your gallery page.";

            return View();
        }

        private static async Task SaveFB(Feedback fb)
        {
            string connectionString = "mongodb://localhost:27017";
            MongoClient client = new MongoClient(connectionString);
            IMongoDatabase database = client.GetDatabase("MonGo"); // MonGo - название базы данных
            var collection = database.GetCollection<BsonDocument>("Feedback");
            await collection.InsertOneAsync(new BsonDocument
            {
                {"Name", fb.Name},
                {"Email", fb.Email},
                {"Subject", fb.Subject},
                {"Body", fb.Body}
            });
        }
}
}